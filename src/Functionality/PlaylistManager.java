package Functionality;

import Database.SQLite;
import GUI.Main;
import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.stream.Collectors;


public class PlaylistManager {
    @FXML
    private JFXTextField playlistNameTxf;
    @FXML
    private JFXListView<Label> PlaylistAvailableListView, PlaylistEditorListView;
    @FXML
    private StackPane rootStackPane;
    private Timer t = null;

    public void initialize() throws SQLException {
        populateList();
    }

    /***
     * Populates the listviews according to the stored database data, and assigns icons depending on filetype.
     * @throws SQLException
     */
    public void populateList() throws SQLException {
        String input = "";
        ArrayList<String> mediaList = new ArrayList<>();

        ResultSet result = SQLite.statement.executeQuery("Select Path from MediaTable where Path LIKE '%" + input + "%'" +
                "OR Tags LIKE '%" + input + "%'" +
                "OR Type LIKE '%" + input + "%'" +
                "OR Genre LIKE '%" + input + "%'" +
                "OR Name LIKE '%" + input + "%';");

        while (result.next()) {
            mediaList.add(result.getString("Path"));
        }
        PlaylistAvailableListView.getItems().clear();

        for (String path : mediaList) {
            Label label = new Label(path.substring(path.lastIndexOf('\\') + 1) + "");

            if (path.contains(".mp3") || path.contains(".wav") || path.contains(".m4a") || path.contains(".aif")
                    || path.contains(".aiff")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.MUSIC));
            } else if (path.contains(".mp4") || path.contains(".m4v") || path.contains(".flv")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.FILM));
            } else {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.QUESTION_CIRCLE));
            }
            PlaylistAvailableListView.getItems().add(label);
        }
    }

    /***
     * Gets the items of the listview containing added videos and gets the string of the labels within in and
     * assigns it to an array that can be used to create the SQL queries.
     *
     * In the SQL flow, it first adds an entry given the user entered name.
     * Afterwards it finds the autoincremented ID of this playlist.
     * It then goes through a forloop with a length equal to the length of the added videos array and creates
     * the playlist content, linking the media table and the playlist table within the database.
     * @throws SQLException
     */
    public void CreatePlaylist() throws SQLException {

        String addedVideosRaw = PlaylistEditorListView.getItems().stream().map(Label::getText).collect(Collectors.joining("|"));
        String[] addedVideos = addedVideosRaw.split("\\|");
        long addedVideoCount = PlaylistEditorListView.getItems().stream().map(Label::getText).count();

        String playlistName = playlistNameTxf.getText();

        if (!playlistName.equals("")) {

            SQLite.statement.executeUpdate("INSERT INTO \"Playlist\" VALUES (null,'" + playlistName + "')");
            ResultSet playlistID = SQLite.statement.executeQuery("SELECT PlaylistID FROM Playlist WHERE Name LIKE '%" + playlistName + "%';");
            String playlistIDString = playlistID.getString(1);

            for (int i = 0; i < addedVideoCount; i++) {
                ResultSet mediaID = SQLite.statement.executeQuery("SELECT MediaID FROM MediaTable WHERE Path LIKE '%" + addedVideos[i] + "%';");
                String mediaIDString = mediaID.getString(1);
                SQLite.statement.executeUpdate("INSERT INTO \"PlaylistContent\" VALUES (" + playlistIDString + "," + mediaIDString + ")");
            }

            // Show dialog
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            JFXDialog dialog = new JFXDialog(rootStackPane, dialogLayout, JFXDialog.DialogTransition.TOP);
            dialogLayout.getStyleClass().add("dialog");
            dialogLayout.setHeading(new Label("Playlist Created!"));
            dialog.show();

            int delay = 1000;

            if (t == null) {
                t = new java.util.Timer();
            }

            t.schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            dialog.close();
                        }
                    },
                    delay
            );
        } else {

            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.getStyleClass().add("dialog");
            JFXDialog dialog = new JFXDialog(rootStackPane, dialogLayout, JFXDialog.DialogTransition.TOP);
            dialogLayout.setHeading(new Label("Please enter a playlist name"));
            dialog.show();

            int delay = 1000;

            if (t == null) {
                t = new java.util.Timer();
            }

            t.schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            dialog.close();
                        }
                    },
                    delay
            );
        }
    }

    /***
     * "Copies" the listview index from the available sources to the playlist listview.
     */
    public void handleFileAdd() {
        ObservableList<Label> media;
        media = PlaylistAvailableListView.getSelectionModel().getSelectedItems();

        for (Label item : media) {
            Label label = new Label(item.toString().substring(item.toString().indexOf('\'') + 1, item.toString().length() - 1));
            if (item.toString().contains(".mp3") || item.toString().contains(".wav") || item.toString().contains(".m4a") || item.toString().contains(".aif")
                    || item.toString().contains(".aiff")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.MUSIC));
            } else if (item.toString().contains(".mp4") || item.toString().contains(".m4v") || item.toString().contains(".flv")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.FILM));
            } else {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.QUESTION_CIRCLE));
            }
            PlaylistEditorListView.getItems().add(label);
        }
    }
    /***
     * Removes the object at the selected index from the playlist listview.
     */
    public void handleFileRemove() {
        final int selectedIndex = PlaylistEditorListView.getSelectionModel().getSelectedIndex();
        PlaylistEditorListView.getItems().remove(selectedIndex);
    }
    /***
     * Spawns the window and controller responsible for adding media, same class as in the SidePanelController.addFile()
     * @Throws IOException, SQLException
     */
    @FXML
    void addFile() throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("metaDataEditor.fxml"));
        BorderPane root = loader.load();

        Stage dialogStage = new Stage();
        dialogStage.setTitle("Add Metadata");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(Main.stage);
        JFXDecorator decorator = new JFXDecorator(dialogStage, root);
        Scene scene = new Scene(decorator);
        scene.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setResizable(false);
        loader.getController();

        dialogStage.showAndWait();
        populateList();
    }
}
