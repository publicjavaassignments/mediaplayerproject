package Functionality;

import Database.SQLite;
import GUI.Main;
import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SidePanelController implements Initializable {

    @FXML
    private JFXListView<Label> files, playlists;

    @FXML
    private JFXPopup popupPlaylists = new JFXPopup();

    @FXML
    private JFXTextField searchVideoTxf, searchPlaylistTxf, nameFileTxf, tagFileTxf, genreFileTxf;

    public static PlayVideoCallback callback;
    public static int mediaCount = 0;
    public static ArrayList<String> playlistPaths;
    public static Playlist playlist;

    public void setCallback(PlayVideoCallback callback) {
        this.callback = callback;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Update files ListView when there is a change in the search field.
        try {
            handleSearchFiles();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        searchVideoTxf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    handleSearchFiles();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        //Update playlist ListView when there is a change in the search field.
        try {
            handleSearchPlaylist();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        searchPlaylistTxf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    handleSearchPlaylist();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        //Show delete popup on playlists.
        initPopupPlaylist();
    }

    /***
     * Creates a delete button popup when right clicked on a playlist, and deletes the selected playlist.
     */
    private void initPopupPlaylist() {
        JFXButton deleteButton = new JFXButton("Delete");
        deleteButton.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.MINUS));

        deleteButton.setOnAction(event ->
        {
            final int selectedIndex = playlists.getSelectionModel().getSelectedIndex();
            String clickedPlaylist = playlists.getItems().get(selectedIndex).getText();
            try {

                SQLite.statement = SQLite.connect.createStatement();
                ResultSet result = SQLite.statement.executeQuery("SELECT PlaylistID FROM Playlist WHERE Name = '" + clickedPlaylist + "';");
                int playlistID = result.getInt("PlaylistID");
                SQLite.statement.executeUpdate("DELETE from PlaylistContent where PlaylistID = +" + playlistID + ";");
                SQLite.statement.executeUpdate("DELETE FROM Playlist WHERE PlaylistID = " + playlistID + ";");
                SQLite.statement.close();
                Controller.mediaPlayer.dispose();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            popupPlaylists.hide();

            try {
                handleSearchPlaylist();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        deleteButton.setStyle("-fx-background-color: #FF8C00");
        VBox vBox = new VBox(deleteButton);
        vBox.setStyle("-fx-background-color: #272727");
        popupPlaylists.setPopupContent(vBox);
    }

    /***
     * Opens the Media Manager window, when "Create Playlist" button is clicked.
     * @throws IOException
     */
    public void openManager() throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("PlaylistManager.fxml"));
        StackPane root = loader.load();

        Stage dialogStage = new Stage();
        dialogStage.setTitle("Playlist Manager");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(Main.stage);
        JFXDecorator decorator = new JFXDecorator(dialogStage, root);
        Scene scene = new Scene(decorator);
        scene.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setResizable(false);
        loader.getController();

        dialogStage.showAndWait();
        handleSearchPlaylist();
    }

    /***
     * Search by name, tags, Type, Genre.
     * Pull data from the database that matches the search input, and update the listView.
     * @throws SQLException
     */
    public void handleSearchFiles() throws SQLException {

        String input = searchVideoTxf.getText();
        ArrayList<String> mediaList = new ArrayList<>();

        ResultSet result = SQLite.statement.executeQuery("Select Path from MediaTable where Path LIKE '%" + input + "%'" +
                "OR Tags LIKE '%" + input + "%'" +
                "OR Type LIKE '%" + input + "%'" +
                "OR Genre LIKE '%" + input + "%'" +
                "OR Name LIKE '%" + input + "%';");

        while (result.next()) {
            mediaList.add(result.getString("Path"));
        }
        files.getItems().clear();

        for (String path : mediaList) {

            Label label = new Label(path.substring(path.lastIndexOf('\\') + 1) + "");

            if (path.contains(".mp3") || path.contains(".wav") || path.contains(".m4a") || path.contains(".aif")
                    || path.contains(".aiff")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.MUSIC));
            } else if (path.contains(".mp4") || path.contains(".m4v") || path.contains(".flv")) {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.FILM));
            } else {
                label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.QUESTION_CIRCLE));
            }
            files.getItems().add(label);
        }
    }

    /***
     * Pull playlist names from the database, which is like the search input, update the listView.
     * @throws SQLException
     */
    public void handleSearchPlaylist() throws SQLException {
        String input = searchPlaylistTxf.getText();

        ArrayList<String> playlistList = new ArrayList<>();

        ResultSet result = SQLite.statement.executeQuery("SELECT Name FROM Playlist WHERE Name LIKE '%" + input + "%';");

        while (result.next()) {
            playlistList.add(result.getString("Name"));
        }

        playlists.getItems().clear();
        for (String playlistName : playlistList) {
            Label label = new Label(playlistName);
            label.setGraphic(new FontAwesomeIconView(FontAwesomeIcon.LIST_OL));
            playlists.getItems().add(label);
        }


    }

    /***
     * Get the name of the selected playlist, call getPlaylistPaths() to get the paths from the selected playlist,
     * and calls playPlaylist to play the playlist.
     * @throws SQLException
     */
    public void handlePlaylistClick(MouseEvent event) throws SQLException {

        if (event.getButton() == MouseButton.SECONDARY) {
            popupPlaylists.show(playlists, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT, event.getX(), event.getY());
        } else {
            playlist = new Playlist();
            String selectedPlaylist = playlists.getSelectionModel().getSelectedItem().toString();
            selectedPlaylist = selectedPlaylist.substring(selectedPlaylist.indexOf('\'') + 1, selectedPlaylist.length() - 1);

            ResultSet result = SQLite.statement.executeQuery("SELECT PlaylistID FROM Playlist WHERE Name = '" + selectedPlaylist + "';");

            int playlistID = result.getInt("PlaylistID");
            playlist.setPlayListID(playlistID);
            playlistPaths = playlist.getPlaylistPaths();
            mediaCount = 0;
            playlist.playPlaylist(playlistPaths);
        }
    }

    /***
     * Get the name of the selected media, pull the path from the database, playVideo() with the selected path.
     * @return
     * @throws SQLException
     */
    public void handleFileClick() throws SQLException {

        SQLite.statement = SQLite.connect.createStatement();
        String selectedMedia = files.getSelectionModel().getSelectedItem().toString();
        selectedMedia = selectedMedia.substring(selectedMedia.indexOf('\'') + 1, selectedMedia.length() - 1);

        ResultSet result = SQLite.statement.executeQuery("SELECT Path FROM MediaTable WHERE Path LIKE '%" + selectedMedia + "%';");

        String path = result.getString("Path");
        SQLite.statement.close();
        callback.playVideo(path);
    }

    /***
     * Spawns the window and controller responsible for adding media.
     * @Throws IOException, SQLException
     */
    @FXML
    void addFile() throws SQLException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("metaDataEditor.fxml"));
        BorderPane root = loader.load();

        Stage dialogStage = new Stage();
        dialogStage.setTitle("Add Metadata");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(Main.stage);
        JFXDecorator decorator = new JFXDecorator(dialogStage, root);
        Scene scene = new Scene(decorator);
        scene.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
        dialogStage.setScene(scene);
        dialogStage.setResizable(false);
        loader.getController();

        dialogStage.showAndWait();
        handleSearchFiles();
    }
}