package Functionality;

import Database.SQLite;
import GUI.Main;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.sql.SQLException;

public class metaDataEditor {

    @FXML
    private JFXTextField nameFileTxf, tagFileTxf, genreFileTxf;

    @FXML
    private JFXButton btnFinalizeAdd;

    private String addedFilePath;

    public void initialize() {
        addedFilePath = handleFileChooser();
    }

    /***
     * Opens a filechooser so the user can add external media to the database.
     * @throws SQLException
     * @return a string of the path to the file the user selected in the filechooser.
     */
    private String handleFileChooser() {
        try {
            FileChooser chooser = new FileChooser();
            chooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Video Files", "*.mp4", "*.m4v", "*.flv"),
                    new FileChooser.ExtensionFilter("Audio Files", "*.m4a", "*.mp3", "*.wav"));

            File addedFile;
            addedFile = chooser.showOpenDialog(Main.stage);
            if (addedFile != null) {
                String addedFilePath = addedFile.toString();

                return addedFilePath;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /***
     * Reads the JFXFields to get values that can be treated and inserted into the SQLite database file.
     * The method will tag the files accordingly depending on filetypes automatically.
     * @throws SQLException
     */
    @FXML
    private void btnFinalizeAdd() throws SQLException {
        String fileName = nameFileTxf.getText().toLowerCase();
        String fileTag = tagFileTxf.getText().toLowerCase();
        String fileGenre = genreFileTxf.getText().toLowerCase();
        String fileType;

        if (addedFilePath.contains(".mp3") || addedFilePath.contains(".wav") || addedFilePath.contains(".m4a") || addedFilePath.contains(".aif")
                || addedFilePath.contains(".aiff")) {
            fileType = "Audio";
        } else if (addedFilePath.contains(".mp4") || addedFilePath.contains(".m4v") || addedFilePath.contains(".flv")) {
            fileType = "Video";
        } else {
            fileType = "Unknown";
        }

        String addEntry = "INSERT INTO \"MediaTable\" VALUES (null,'" + addedFilePath + "','" + fileName + "','" + fileTag + "','" + fileType + "','" + fileGenre + "')";
        SQLite.statement.executeUpdate(addEntry);
        SQLite.statement.close();

        Stage stage = (Stage) btnFinalizeAdd.getScene().getWindow();
        stage.close();
    }
}
