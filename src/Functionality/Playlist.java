package Functionality;

import Database.SQLite;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Playlist {

    public int playListID;


    public void setPlayListID(int playListID) {
        this.playListID = playListID;
    }

    public int getPlayListID() {
        return playListID;
    }

    /***
     * Returns an ArrayList containing paths from the media in the selected playlist.
     * @return
     * @throws SQLException
     */
    public ArrayList<String> getPlaylistPaths() throws SQLException {
        ArrayList<String> playlistPaths = new ArrayList<>();

        SQLite.statement = SQLite.connect.createStatement();
        ResultSet result;

        String sql = ("Select * from MediaTable where MediaID in (SELECT MediaID from PlaylistContent where PlaylistID = " + getPlayListID() + ");");
        result = SQLite.statement.executeQuery(sql);

        while (result.next()) {
            String path = result.getString("Path");
            playlistPaths.add(path);
        }
        return playlistPaths;
    }

    /***
     * Takes the playlistPaths ArrayList as parameter, calls playVideo(), and plays the Media at index mediaCount.
     * @param playlistPaths
     */
    public void playPlaylist(ArrayList<String> playlistPaths) {
        if (SidePanelController.mediaCount == playlistPaths.size()) {
            Controller.mediaPlayer.dispose();
        }
        SidePanelController.callback.playVideo(playlistPaths.get(SidePanelController.mediaCount));

        Controller.mediaPlayer.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                SidePanelController.mediaCount++;
                playPlaylist(playlistPaths);
            }
        });
    }
}
