package Functionality;

import Functionality.utilities.FileHandlingUtilities;
import GUI.Main;
import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller implements Initializable, PlayVideoCallback {
    @FXML
    private StackPane rootStackPane;
    @FXML
    private MediaView mediaView;
    @FXML
    private HBox mediaControl;
    @FXML
    private Label timeLineLabel;
    @FXML
    private Pane mediaViewPane;
    @FXML
    private FontAwesomeIconView PlayButtonIcon, SpeakerButton;
    @FXML
    private JFXSlider volumeSlider, timeSlider;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private ImageView logo;

    private Timer t = null;
    static MediaPlayer mediaPlayer;

    private double previousVolume;
    private boolean stopRequested = false;
    private boolean atEndOfMedia = false;
    private FadeTransition ft;

    // BEGIN INTERNALS ----------------------------------------------------------------------------------------------
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/sidepanel.fxml"));
            VBox box = loader.load();
            box.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
            drawer.setSidePane(box);
            SidePanelController sidePanelController = loader.getController();
            sidePanelController.setCallback(this);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        mediaView.fitWidthProperty().bind(mediaViewPane.widthProperty());
        mediaView.fitHeightProperty().bind(mediaViewPane.heightProperty());
        volumeSlider.setValue(100);
        timeSlider.setValue(0);

        ft = new FadeTransition(Duration.millis(1000), mediaControl);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setCycleCount(1);
    }

    /***
     * Takes a path as parameter, creates a new mediaplayer and plays the video.
     * @param MediaPath
     */
    public void playVideo(String MediaPath) {
        try {
            String MediaURLEncoded = URLEncoder.encode(MediaPath, "UTF-8");
            MediaURLEncoded = "file:/"
                    + (MediaURLEncoded).replace("\\", "/").replace("+", "%20");
            Media media = new Media(MediaURLEncoded);

            // Set the background image according to filetype
            if (MediaPath.contains("mp3") || MediaPath.contains("wav") || MediaPath.contains("m4a")) {
                logo.setImage(new Image("/resources/audio.png"));
            } else if (MediaPath.contains("mp4") || MediaPath.contains("m4v") || MediaPath.contains("flv")) {
                logo.setImage(new Image("/resources/logo.png"));
            }

            //Check if the previous mediaplayer exists
            checkAndStopMediaPlayer();

            //Create new mediaplayer
            mediaPlayer = new MediaPlayer(media);
            mediaView.setMediaPlayer(mediaPlayer);
            bindMediaPlayerControls(mediaPlayer);
            mediaPlayer.setAutoPlay(true);
            mediaPlayer.play();
        } catch (Exception e) {
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            JFXDialog dialog = new JFXDialog(rootStackPane, dialogLayout, JFXDialog.DialogTransition.TOP);
            dialog.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
            dialogLayout.getStyleClass().add("dialog");
            dialogLayout.setHeading(new Label("Cannot play this media!"));
            dialog.show();

            int delay = 1000;

            if (t == null) {
                t = new java.util.Timer();
            }

            t.schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            dialog.close();
                        }
                    },
                    delay
            );
            e.printStackTrace();
        }
    }

    /***
     * Checks if a mediaplayer exists and removes it.
     */
    private void checkAndStopMediaPlayer() {
        if (null != mediaView.getMediaPlayer()) {
            stopAction(null);
            mediaView.setMediaPlayer(null);
        }
    }

    /***
     *  Takes duration and mediaplayer as parameters, and format the timestamp.
     * @param Duration
     * @param mediaPlayer
     * @return
     */
    public String timestamp(int Duration, MediaPlayer mediaPlayer) {
        int totalTime = (int) mediaPlayer.getTotalDuration().toSeconds();

        if (totalTime > 3600) {
            return LocalTime.MIN.plusSeconds(Duration).toString();
        } else {
            return LocalTime.MIN.plusMinutes(Duration).toString();
        }
    }

    /***
     *  Since the MediaViewer requires a MediaPlayer object, which then holds the timeline duration, volumecontrols, etc
     *  it's necesarry to attach our UI controls to the new MediaPlayer object, whenever it is destroyed and recreated.
     *
     *  This takes a mediaplayer object, and attaches the controls of it to the UI.
     * @param mediaPlayer
     */
    private void bindMediaPlayerControls(final MediaPlayer mediaPlayer) {
        updateValues(mediaPlayer);

        mediaPlayer.setOnPlaying(() -> {
            if (stopRequested) {
                mediaPlayer.pause();
                stopRequested = false;
            } else {
                PlayButtonIcon.setIcon(FontAwesomeIcon.PAUSE);
            }
        });

        mediaPlayer.setOnPaused(() ->
        {
            PlayButtonIcon.setIcon(FontAwesomeIcon.PLAY);
        });

        volumeSlider.valueProperty().addListener((ov) -> {
            if (null != mediaPlayer) {
                if (volumeSlider.getValue() > 0) {
                    if (volumeSlider.getValue() > 50) {
                        SpeakerButton.setIcon(FontAwesomeIcon.VOLUME_UP);
                    } else {
                        SpeakerButton.setIcon(FontAwesomeIcon.VOLUME_DOWN);
                    }
                } else if (volumeSlider.getValue() == 0) {
                    SpeakerButton.setIcon(FontAwesomeIcon.VOLUME_OFF);
                }
                mediaPlayer.setVolume(volumeSlider.getValue() / 100.0);
            } else {
                volumeSlider.setValue(0);
            }
        });
    }

    /***
     * Takes a mediaplayer to update values related to the MediaPlayer object,
     * ensuring volume, timeline, pause & stop work as expected upon playing a new
     * media object.
     * @param mediaPlayer
     */
    private void updateValues(MediaPlayer mediaPlayer) {
        if (timeLineLabel != null && timeSlider != null && volumeSlider != null) {
            InvalidationListener sliderChangeListener = o -> {
                Duration seekTo = Duration.seconds(timeSlider.getValue());
                mediaPlayer.seek(seekTo);
            };
            timeSlider.valueProperty().addListener(sliderChangeListener);

            mediaPlayer.currentTimeProperty().addListener(l -> {
                timeSlider.valueProperty().removeListener(sliderChangeListener);

                Duration currentTime = mediaPlayer.getCurrentTime();
                double value = currentTime.toSeconds();
                timeSlider.setValue(value);
                timeSlider.setMax(mediaPlayer.getTotalDuration().toSeconds());
                timeSlider.valueProperty().addListener(sliderChangeListener);

                //Set timestamp Label
                String duration = timestamp((int) value, mediaPlayer);
                int totalTime = (int) mediaPlayer.getTotalDuration().toSeconds();
                String totalDuration = timestamp(totalTime, mediaPlayer);
                timeLineLabel.setText(duration + " / " + totalDuration);
            });
        }
    }
    // END INTERNALS ------------------------------------------------------------------------------------------------

    /***
     * Opens and closes sidepanel
     */
    // User interface actions
    public void handleDrawer() {
        if (drawer.isOpened()) {
            drawer.close();
        } else {
            drawer.open();
        }
    }

    @FXML
    /***
     * Handle playbutton.
     */
    void playAction(ActionEvent event) {
        MediaPlayer mediaPlayer = mediaView.getMediaPlayer();
        if (null != mediaPlayer) {
            MediaPlayer.Status status = mediaPlayer.getStatus();
            if (status == MediaPlayer.Status.UNKNOWN || status == MediaPlayer.Status.HALTED) {
                return;
            }

            if (status == MediaPlayer.Status.PAUSED || status == MediaPlayer.Status.READY
                    || status == MediaPlayer.Status.STOPPED) {
                // LOOP HANDLING
                if (atEndOfMedia) {
                    mediaPlayer.seek(mediaPlayer.getStartTime());
                    atEndOfMedia = false;
                }
                mediaPlayer.play();
                PlayButtonIcon.setIcon(FontAwesomeIcon.PAUSE);
            } else {
                mediaPlayer.pause();
                PlayButtonIcon.setIcon(FontAwesomeIcon.PLAY);
            }
        } else {
            event.consume();
        }
    }

    @FXML
    /***
     * Handle stop button
     */
    void stopAction(ActionEvent event) {
        MediaPlayer mediaPlayer = mediaView.getMediaPlayer();
        if (null != mediaPlayer) {
            mediaPlayer.stop();
            timeSlider.setValue(0);
            PlayButtonIcon.setIcon(FontAwesomeIcon.PLAY);
        } else {
            event.consume();
        }
    }

    /***
     * Handle forward button. Plays the next video in the selected playlist
     */
    public void handleForwardBtn() {
        SidePanelController.mediaCount++;
        SidePanelController.playlist.playPlaylist(SidePanelController.playlistPaths);
    }

    /***
     * Handle forward button. Plays the previous video in the selected playlist
     */
    public void handleBackwardBtn() {
        SidePanelController.mediaCount--;
        SidePanelController.playlist.playPlaylist(SidePanelController.playlistPaths);
    }

    @FXML
    /***
     * Handle volume mute/unmute button
     */
    void muteUnmute() {
        if (volumeSlider.getValue() == 0) {
            volumeSlider.setValue(previousVolume);
            SpeakerButton.setIcon(FontAwesomeIcon.VOLUME_UP);
        } else {
            previousVolume = volumeSlider.getValue();
            volumeSlider.setValue(0);
            SpeakerButton.setIcon(FontAwesomeIcon.VOLUME_OFF);
        }
    }

    /***
     * Display audio slider when hovered over. Set timer to 4 seconds.
     */
    public void handleAudioSlider() {
        ScaleTransition enter = new ScaleTransition(Duration.millis(175), volumeSlider);
        enter.setToY(1f);
        enter.setToX(1f);
        enter.play();
        int delay = 4000;

        if (t == null) {
            t = new java.util.Timer();
        }
        t.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        handleAudioSliderFade();
                    }
                },
                delay
        );
    }

    /***
     * Fade audioslider.
     */
    public void handleAudioSliderFade() {
        ScaleTransition exit = new ScaleTransition(Duration.millis(175), volumeSlider);
        exit.setToY(0f);
        exit.setToX(0f);
        exit.play();
    }

    /***
     *  Creates a dragboard on the PrimaryStage, which can have files dragged onto it.
     *  The path for the given file which was dragged onto the dragboard is then extracted, and the path is fed
     *  to the PlayVideo method for instant playing.
     * @param scene
     */
    public void dragAndDrop(Scene scene) {
        try {
            scene.setOnDragOver((dragEvent) -> {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    dragEvent.acceptTransferModes(TransferMode.COPY);
                } else {
                    dragEvent.consume();
                }
            });

            scene.setOnDragDropped((dragEvent) ->
            {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    for (Path filePath : FileHandlingUtilities.convertListFiletoListPath(db.getFiles())) {
                        try {
                            playVideo(filePath.toAbsolutePath().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
