package Functionality.utilities;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
/***
 * Easier handling of File objects.
 * @Return takes a File object and returns the path of it to a string.
 */
public class FileHandlingUtilities {
    public static List<Path> convertListFiletoListPath(List<File> listOfFile) {
        return listOfFile.stream().map(File::toPath).collect(Collectors.toList());
    }
}
