CREATE TABLE IF NOT EXISTS "MediaTable"
(
    "MediaID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "Path"    TEXT,
    "Name"    TEXT,
    "Tags"    TEXT,
    "Type"    TEXT,
    "Genre"   TEXT
);

CREATE TABLE IF NOT EXISTS "PlaylistContent"
(
    "PlaylistID" INTEGER,
    "MediaID"    INTEGER,
    FOREIGN KEY ("PlaylistID") REFERENCES "Playlist" ("PlaylistID"),
    FOREIGN KEY ("MediaID") REFERENCES "MediaTable" ("MediaID")
);

CREATE TABLE IF NOT EXISTS "Playlist"
(
    "PlaylistID" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
    "Name"       TEXT
);

CREATE TABLE IF NOT EXISTS "UserSettings"
(
    "Volume"   INTEGER,
    "Shuffle"  INTEGER,
    "Repeat"   INTEGER,
    "Autoplay" INTEGER
);