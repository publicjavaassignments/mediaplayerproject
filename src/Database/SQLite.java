package Database;

import GUI.Main;

import java.io.File;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLite {

    public static final String DBFILENAME = "MediaPlayer.db";
    public static Connection connect;
    public static Statement statement;

    public static void init() throws SQLException {
        if (!new File(Main.CONFIG_DATADIR).exists()) {
            new File(Main.CONFIG_DATADIR).mkdir();
        }
        connect();
    }

    public static void setupDatabase() throws SQLException {
        try {
            MediaManagerSQL.setupDatabase();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public static Connection getConnection() {
        return connect;
    }

    public static void connect() {
        try {
            connect = DriverManager.getConnection("jdbc:sqlite:" + Main.CONFIG_DATADIR + "/" + SQLite.DBFILENAME);
            if (connect != null) {
            }
        } catch (SQLException ex) {
            System.err.println("Kunne ikke forbinde til database\n" + ex.getMessage());
        }
    }

    public static void close() {
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
