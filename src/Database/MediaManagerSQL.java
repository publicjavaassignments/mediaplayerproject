package Database;

import java.io.File;
import java.sql.SQLException;

public class MediaManagerSQL {

    /***
     * Create the database with tables: MediaTable, PlaylistContent, Playlist, UserSettings.
     * @throws SQLException
     */
    public static void setupDatabase() throws SQLException {
        try {
            SQLite.statement = SQLite.connect.createStatement();
            String databaseCreation = "CREATE TABLE IF NOT EXISTS \"MediaTable\" (\n" +
                    "\t\"MediaID\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "\t\"Path\"\tTEXT,\n" +
                    "\t\"Name\"\tTEXT,\n" +
                    "\t\"Tags\"\tTEXT,\n" +
                    "\t\"Type\"\tTEXT,\n" +
                    "\t\"Genre\"\tTEXT\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE IF NOT EXISTS \"PlaylistContent\" (\n" +
                    "\t\"PlaylistID\"\tINTEGER,\n" +
                    "\t\"MediaID\"\tINTEGER,\n" +
                    "\tFOREIGN KEY(\"PlaylistID\") REFERENCES \"Playlist\"(\"PlaylistID\") ON DELETE CASCADE,\n" +
                    "\tFOREIGN KEY(\"MediaID\") REFERENCES \"MediaTable\"(\"MediaID\") ON DELETE CASCADE\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE IF NOT EXISTS \"Playlist\" (\n" +
                    "\t\"PlaylistID\"\tINTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                    "\t\"Name\"\tTEXT\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE IF NOT EXISTS \"UserSettings\" (\n" +
                    "\t\"Volume\"\tINTEGER DEFAULT 100 NOT NULL,\n" +
                    "\t\"Shuffle\"\tINTEGER DEFAULT 1 NOT NULL,\n" +
                    "\t\"Repeat\"\tINTEGER DEFAULT 1 NOT NULL,\n" +
                    "\t\"Autoplay\"\tINTEGER DEFAULT 1 NOT NULL\n" +
                    ");";

            SQLite.statement.executeUpdate(databaseCreation);
            SQLite.statement.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    /***
     * Populate database tables with media from the media folder.
     * @throws SQLException
     */
    public static void populateDatabaseMedia() throws SQLException {
        String path = new File("src/Media").getAbsolutePath();
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String addMediaPath = "";

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                SQLite.statement = SQLite.connect.createStatement();
                String clearPreviousDatabase = "DELETE FROM \"main\".\"MediaTable\" WHERE _rowid_ IN ('" + i + "')";

                if (listOfFiles[i].getName().contains(".mp3") || listOfFiles[i].getName().contains(".wav") || listOfFiles[i].getName().contains(".m4a") || listOfFiles[i].getName().contains(".aif")
                        || listOfFiles[i].getName().contains(".aiff")) {
                    addMediaPath = "INSERT INTO \"MediaTable\" VALUES (" + i + ",'" + listOfFiles[i].getAbsolutePath() + "','" + listOfFiles[i].getName() + "','Music Audio','Audio','Audio');";
                } else if (listOfFiles[i].getName().contains(".mp4") || listOfFiles[i].getName().contains(".m4v") || listOfFiles[i].getName().contains(".flv")) {
                    addMediaPath = "INSERT INTO \"MediaTable\" VALUES (" + i + ",'" + listOfFiles[i].getAbsolutePath() + "','" + listOfFiles[i].getName() + "','Video','Video','Video');";
                } else {
                    addMediaPath = "INSERT INTO \"MediaTable\" VALUES (" + i + ",'" + listOfFiles[i].getAbsolutePath() + "','" + listOfFiles[i].getName() + "','Unknown','Unknown','Unknown');";
                }
                SQLite.statement.executeUpdate(clearPreviousDatabase);
                SQLite.statement.executeUpdate(addMediaPath);
                SQLite.statement.close();
            }
        }
    }
}
