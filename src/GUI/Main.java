package GUI;

import Database.MediaManagerSQL;
import Database.SQLite;
import Functionality.Controller;
import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
    public static final String CONFIG_DATADIR = "PlayerData";

    public static Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));
        Parent root = loader.load();
        JFXDecorator decorator = new JFXDecorator(stage, root);
        decorator.setCustomMaximize(false);
        Scene scene = new Scene(decorator);
        decorator.setCustomMaximize(true);
        Controller controller = loader.getController();
        stage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("Theme.css").toExternalForm());
        stage.setTitle("Scuffed Media Player");
        stage.show();
        controller.dragAndDrop(scene);
    }

    public static void main(String[] args) throws Exception {
        SQLite.init();
        SQLite.setupDatabase();
        MediaManagerSQL.populateDatabaseMedia();
        launch(args);
    }
}
